# Test utility to check if AWS credentials are properly installed.

### Usage
  - run `mvn test` from the command line
  - If any exceptions are thrown you can use them to debug the issue. 
  
###Running the main method in the App class will do crud-like tests on a few services you should have permissions to  
  - read an AWS Systems Manager Parameter Store parameter
  - write a new version of the parameter to the Store with your user information
  - reread the parameter 
  - read a file from AWS S3
  - overwrite the file with your user information
  - reread the file


# Issues we have encountered
  - credentials not loaded
    - try restarting your machine
    - make sure the credentials file is in the proper location - the  `.aws` folder should be located just inside your user folder
    - the credentials file should be named `credentials` with no file extension
  - bad contents
    - the file should contain 
      - ```
        [default]
        aws_access_key_id = your_access_key_id
        aws_secret_access_key = your_secret_access_key
        ```
         where `your_access_key_id` and  `your_secret_access_key` represent a valid aws token
   