package org.fastbridge;

import org.apache.commons.io.IOUtils;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagement;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClientBuilder;
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterRequest;
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterResult;
import com.amazonaws.services.simplesystemsmanagement.model.PutParameterRequest;

/**
 * Hello world!
 */
public class App {
    private static AWSSimpleSystemsManagement simpleSystemsManagementClient;

    private static final String TEST_PARAMETER_NAME = "/DEV/TEST_NAMESPACE/PARAMETER_NAME/KEY";
    private static final String AWS_BUCKET_FBL_FILESTORE = "fbl-filestore";
    private static final String AWS_FILE_PATH = "testPath/credentialFile.txt";

    public static void main(String[] args) {
        System.out.println("Hello World!");
        initialize();
        System.out.println("Current parameter value: " + getSecureParameter());
        setSecureParameter();
        System.out.println("Updated parameter value: " + getSecureParameter());
        System.out.println("Current file value: " + readFile());
        writeFile();
        System.out.println("Updated file value: " + readFile());
    }

    private static void setSecureParameter() {
        PutParameterRequest parameterRequest = new PutParameterRequest();
        parameterRequest.withName(TEST_PARAMETER_NAME);
        parameterRequest.withValue(getUniqueParameterName());
        parameterRequest.withOverwrite(true);
        parameterRequest.withType(com.amazonaws.services.simplesystemsmanagement.model.ParameterType.SecureString);
        simpleSystemsManagementClient.putParameter(parameterRequest);
    }

    private static String getSecureParameter() {
        try {
            GetParameterRequest parameterRequest = new GetParameterRequest();
            parameterRequest.withName(TEST_PARAMETER_NAME);
            parameterRequest.setWithDecryption(true);
            GetParameterResult parameterResult = simpleSystemsManagementClient.getParameter(parameterRequest);
            return parameterResult.getParameter()
                    .getValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static String getUniqueParameterName() {
        return System.getProperty("user.name") + " @ " + System.currentTimeMillis();
    }

    private static void writeFile() {
        getAmazonS3().putObject(AWS_BUCKET_FBL_FILESTORE, AWS_FILE_PATH, getUniqueFileContents());
    }

    private static String readFile() {
        try {
            return IOUtils.toString(getAmazonS3().getObject(AWS_BUCKET_FBL_FILESTORE, AWS_FILE_PATH)
                    .getObjectContent(), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static AmazonS3 getAmazonS3() {
        return AmazonS3ClientBuilder.standard()
                .withRegion(Regions.US_WEST_2)
                .withClientConfiguration(getAWSClientConfiguration())
                .build();
    }

    private static String getUniqueFileContents() {
        return "File created by: " + getUniqueParameterName();
    }

    private static ClientConfiguration getAWSClientConfiguration() {
        return new ClientConfiguration()
                .withSocketTimeout(0)
                .withConnectionTimeout(0)
                .withMaxConnections(500);
    }

    private static void initialize() {
        simpleSystemsManagementClient = AWSSimpleSystemsManagementClientBuilder
                .standard()
                .withRegion("us-east-2")
                .build();
    }
}
